<?php

/**
 * @file
 * Lists available colors and color schemes for the Bartik theme.
 */

$info = [
    // Available colors and color labels used in theme.
    'fields' => [
        'toolbarbg' => t('Toolbar background color'),
        'toolbartraybg' => t('Toolbar tray background color'),
        'toolbartrayitembg' => t('Toolbar tray item background color'),
    ],
    // Pre-defined color schemes.
    'schemes' => [
        'default' => [
            'title' => t('Black (default)'),
            'colors' => [
                'toolbarbg' => '#000011',
                'toolbartraybg' => '#000012',
                'toolbartrayitembg' => '#000013',
            ],
        ],
        'firehouse' => [
            'title' => t('Firehouse'),
            'colors' => [
                'toolbarbg' => '#FF0000',
                'toolbartraybg' => '#FF0000',
                'toolbartrayitembg' => '#FF0000',
            ],
        ],
        'ice' => [
            'title' => t('Ice'),
            'colors' => [
                'toolbarbg' => '#016b84',
                'toolbartraybg' => '#016b84',
                'toolbartrayitembg' => '#016b84',
            ],
        ],
        'plum' => [
            'title' => t('Plum'),
            'colors' => [
                'toolbarbg' => '#2c2c27',
                'toolbartraybg' => '#2c2c27',
                'toolbartrayitembg' => '#2c2c27',
            ],
        ],
        'slate' => [
            'title' => t('Slate'),
            'colors' => [
                'toolbarbg' => '#161616',
                'toolbartraybg' => '#161616',
                'toolbartrayitembg' => '#161616',
            ],
        ],
    ],

    // CSS files (excluding @import) to rewrite with new color scheme.
    'css' => [
        'css/colors.css',
    ],

    // Files to copy.
    'copy' => [
        'logo.svg',
    ],

    // Gradient definitions.
    'gradients' => [
        [
            // (x, y, width, height).
            'dimension' => [0, 0, 0, 0],
            // Direction of gradient ('vertical' or 'horizontal').
            'direction' => 'vertical',
            // Keys of colors to use for the gradient.
            'colors' => ['top', 'bottom'],
        ],
    ],

    // Preview files.
    'preview_library' => 'wax_adminimal/color.preview',
    'preview_html' => 'color/preview.html',

    // Attachments.
    '#attached' => [
        'drupalSettings' => [
            'color' => [
                // Put the logo path into JavaScript for the live preview.
                'logo' => theme_get_setting('logo.url', 'wax_basetheme'),
            ],
        ],
    ],
];
